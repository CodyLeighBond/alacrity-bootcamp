<?php
  require_once('navbar_top.html');
  require_once('navbar_left.html');
 ?>

<main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
    <section class="row text-center placeholders">
      <div class="col-6 col-sm-3 placeholder person-header">
        <h2>Mum</h2>
        <div class="text-muted">Budget: £100</div>
      </div>
    </section>

    <h3 class="text-dark">Gift Options</h3>

    <table id="table" class="table table-hover table-mc-light-blue">
      <thead>
        <tr>
          <th>Item</th>
          <th>Price</th>
          <th>Link</th>
          <th>Select</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td data-title="Item">Bourbon Candle</td>
          <td data-title="Price">23.99</td>
          <td data-title="Link">
            <a href="http://www.yankeecandle.co.uk/product/bourbon-wood-barrels/_/R-1556263E112" target="_blank">Yankee Candle</a>
          </td>
          <td data-title="Select"></td>
        </tr>
      </tbody>
    </table>
    <br>
    <br>
    <h3 class="text-dark">Suggested Selections</h1>

    <table class="table">
      <table id="table" class="table table-hover table-mc-light-blue">
      <thead>
        <tr>
          <th>Item</th>
          <th>Price</th>
          <th>Link</th>
          <th>Select</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td data-title="Item">Bourbon Candle</td>
          <td data-title="Price">23.99</td>
          <td data-title="Link">
            <a href="http://www.yankeecandle.co.uk/product/bourbon-wood-barrels/_/R-1556263E112" target="_blank">Yankee Candle</a>
          </td>
          <td data-title="Select"></td>
        </tr>
      </tbody>
    </table>
