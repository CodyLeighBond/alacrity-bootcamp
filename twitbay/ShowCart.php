<?php

  class ShowCart {

    public $userID;

    function __construct(int $userID) {
      $this->userID = $userID;
    }

    public function showCartDetails() {
      require_once('Query.php');

      $sql = "SELECT cart.item_name, cart.item_value
              FROM cart
              WHERE cart.userID = $this->userID";

        $query = new Query($sql);
        $cartArray = $query->sqlResult($query->sql);

      if ($cartArray) {
        ?>
        <h1>
          <span>Your cart:</span>
        </h1>
        <?php
          //loop over all records
          for ($i=0; $i < count($cartArray); $i++) {
            echo "<li> {$cartArray[$i]['item_name']}: £{$cartArray[$i]['item_value']} </li>";
          }
        }
      else {
        echo "Could not connect to DB";
      }
    }
  }
 ?>
