<?php
  class Stats {

    function __construct(){

    }

    function showStats(){

      require_once('Query.php');

      $sql = 'select * from twitbay.site_stats';
      $query = new Query($sql);
      $statsArray = $query->sqlResult($query->sql);

       ?>
       <div class="component">
         <div class="dashboard-profile-annotations clearfix">
           <h2 class="dashboard-profile-title">
             <img src="twitbay_files/megaphone.png" alt="Acme Rocket Bikes" class="profile-dashboard" width="24">
               Twitbay Stats
           </h2>
         </div>
      <ul class="user-stats clearfix">
            <li>
              <a class="user-stats-count"><?= "{$statsArray[0]['views']}";?><span class="user-stats-stat">Views</span></a>
            </li>
            <li>
              <a class="user-stats-count"><?= "{$statsArray[0]['posts']}";?><span class="user-stats-stat">Posts</span></a>
            </li>
        <li>
          <a class="user-stats-count"><?= "{$statsArray[0]['sales']}";?><span class="user-stats-stat">Sales</span></a>
        </li>
      </ul>
      <?php
    }
  }
?>
