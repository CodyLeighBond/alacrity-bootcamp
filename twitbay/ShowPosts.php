<?php
class ShowPosts {

  public $posts;

  function __construct(array $posts) {
    $this->posts = $posts;
  }

  public function showAllPosts(array $posts) {
    if ($posts) {
      //loop over all records
      for ($i=0; $i < count($posts) ; $i++) {
        $imgURL = 'twitbay_files/'."{$posts[$i]['userID']}".'.jpg';
        ?>
        <div media="true" class="js-stream-item stream-item">
          <div class="stream-item-content tweet js-actionable-tweet stream-tweet">
            <div class="tweet-image">
              <img src="<?=$imgURL?>" alt="Post pic" class="user-profile-link" height="48" width="48">
            </div>
            <div class="tweet-content">
              <a class="tweet-screen-name user-profile-link"> <?= "{$posts[$i]['username']}";?></a>
              <p class="tweet-text js-tweet-text"> <? echo "{$posts[$i]['content']}"?></p>
              <p class="tweet-timestamp"><? echo "Posted at: {$posts[$i]['time_posted']}"?></p>
            </div>
          </div>
        </div>
      <?php
      }
    }
    else {
      echo "Could not connect to DB";
   }

  }


} ?>
