<?php

  class ListUsers {

    public $users;

    function __construct(array $users) {
      $this->users = $users;
    }

    public function listAllUsers(array $users) {
      if ($users) {
        ?>
        <?php
        //loop over all records
        for ($i=0; $i < count($users); $i++) {
          echo "<li> {$users[$i]['username']} </li>";
        }
      }
      else {
        echo "Could not connect to DB";
      }
    }
  }
?>
