<?php

    class HeaderContent {

    public $headerContentType;

    function __construct(string $headerContentType) {
      $this->headerContentType = $headerContentType;
    }

    public function getHeaderContent(string $headerContentType) {
      if ($this->headerContentType == 'loggedOut') {
        ?>

        <form action="index.php" method="POST">
          Username: <input type="text" name="username" />
          Password: <input type="text" name="password" />
          <input type="submit" value="Sign in" name='sign in' />
        </form>

        <?php
      }
      elseif ($this->headerContentType == 'loggedIn') {
        echo "Welcome!";
      }
      elseif ($this->headerContentType == 'invalid') {
        echo "Incorrect username or password";
      }
    }
  }

?>
