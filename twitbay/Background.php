<?php
  class Background {

    function __construct() {}

    public function showImage() {
      // random background image
      // 4 Available backgrounds
      $randInt = rand(1, 4);
      $imgURL = 'twitbay_files/background'.$randInt.'.jpg';
      ?>

      <style id="user-style-Hicksdesign-bg-img">
        body.user-style-Hicksdesign { background-image: url(<?=$imgURL?>);}
      </style>

      <?php
    }
  }
 ?>
