<!DOCTYPE HTML>
<html class=" js">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Twitbay</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="utf-8">
		<link href="http://twitter.com/phoenix/iefavicon.ico" rel="shortcut icon" type="image/x-icon">
		<link rel="stylesheet" href="twitbay_files/phoenix_core_logged_out.css" type="text/css" media="screen">
		<style charset="utf-8" class="lazyload">
			@import "twitbay_files/phoenix_more.bundle.css";
		</style>

		<!-- random background image -->
		<?php
		require_once('Background.php');

			$background = new Background();
			$background->showImage();
		 ?>
		<!-- End of BG image -->

	</head>
	<body class="logged-out  mozilla user-style-Hicksdesign">
		<div class="route-profile" id="doc">
        	<div id="top-stuff">
          	<div id="top-bar-outer">
          		<div id="top-bar-bg"></div>
                <div id="top-bar">
                <div style="left: 0px;" class="top-bar-inside">
                	<div class="static-links">
    					<div id="logo">
        					<a href="http://localhost/">
        						<img src="twitbay_files/logo.png" alt="nope"/>
        					</a>
    					</div>
    				</div>
    				<div class="active-links">
        				<div id="session">

        				    <!-- TODO - if not logged in, display login form -->
        						<!-- If the user IS logged in then display a welcome message. -->
										<?php
											// Start the session
											session_start();

											// Set session variables
											$_SESSION["headerContentType"] = 'loggedOut';

											require_once('Query.php');
											require_once('HeaderContent.php');

											$sql = 'select * from twitbay.users order by username';
										  $query = new Query($sql);
											$userArray = $query->sqlResult($query->sql);

											$headerContent = new HeaderContent($_SESSION['headerContentType']);
											$headerContent->getHeaderContent($_SESSION['headerContentType']);


											if (isset($_POST["username"]) && isset($_POST["password"])) {

												for ($i=0; $i < count($userArray) ; $i++) {
													if ($_POST['username'] == $userArray[$i]['username'] && $_POST['password'] == $userArray[$i]['password']) {
														$_SESSION['headerContentType'] = 'loggedIn';
														$headerContent = new HeaderContent($_SESSION['headerContentType']);
														$headerContent->getHeaderContent($_SESSION['headerContentType']);
														break;
													}
													if ($i == count($userArray)-1 && $_SESSION['headerContentType'] != 'loggedIn') {
														$_SESSION['headerContentType'] = 'invalid';
														$headerContent = new HeaderContent($_SESSION['headerContentType']);
													 	$headerContent->getHeaderContent($_SESSION['headerContentType']);
													 	break;
													 }
												}
												$_SESSION['userID'] = $i;
												// unset($_POST["username"]);
												// unset($_POST["password"]);
											}
											$userID = $_SESSION['userID'];
										?>
        					    <!-- end of login -->
        				</div>
    				</div>
                </div>
              </div>
            </div>
        </div>
        <div id="page-outer">
          	<div class="profile-container" id="page-container">
          		<div>
          			<div style="min-height: 683px;" class="main-content">
    					<div class="profile-header">
    							<div class="profile-info clearfix">
       	 							<div class="profile-image-container">
          							   <img src="twitbay_files/megaphone.png" alt="Twitbay">
        							</div>
        							<div class="profile-details">
                      	<div class="full-name">
      										<h2>Twitbay Listings</h2>
      									</div>
          						  <div class="screen-name-and-location">
          								<span class="screen-name screen-name-Hicksdesign pill">@Twitbay</span>
          							   	Cardiff, Wales, UK
          						  </div>
          						  <div class="bio">
          							   Only the finest products, services, bric-a-brac and budget bargains - all served
              						   with a dash of ASP.NET goodness...
              					</div>
      						      <div class="url">
                        	<a target="_blank" rel="me nofollow" href="http://www.spiderstudios.co.uk/">http://www.spiderstudios.co.uk/</a>
												</div>
                      </div>
  									</div>
    								<ul class="stream-tabs">
                    	<li class="stream-tab stream-tab-tweets active">
        					   		<a class="tab-text">Posts</a>
                      </li>
					   				</ul>
            	</div>
	    				<div class="stream-manager">
	  						<div class="js-stream-manager" id="profile-stream-manager">
	  							<div class="stream-container">
	  								<div class="stream stream-user">
	  									<div id="stream-items-id" class="stream-items">

	  										<!-- 1. show all posts -->
												<?php
													require_once('Query.php');
													require_once('ShowPosts.php');

													$sql = 'SELECT posts.postID, posts.content, posts.time_posted, users.username, posts.userID
																	FROM posts
																	INNER JOIN users ON users.userID=posts.userID
																	ORDER BY time_posted desc';
												  $query = new Query($sql);
													$postArray = $query->sqlResult($query->sql);
													$posts = new ShowPosts($postArray);
													$posts->showAllPosts($postArray);
												?>

	  										<!-- TODO 2. show only those posts from user in URL -->

	  										<!-- TODO - start of a posting - note that all postings will need to be created from DB -->

											<!-- end of item -->

										</div>
									</div>
								</div>
							</div>
						</div>
  				</div>
				<div class="dashboard profile-dashboard">
          			<div class="component">
          				<div class="signup-call-out">
										<h1>
											<span>Twitbay Users</span>
										</h1>
										<h2>Don't miss products from your favourite users!</h2>

        						<!-- List all Twitbay users in DB -->
										<?php
											require_once('ListUsers.php');
											require_once('Query.php');
										  //select all users
										  $sql = 'select * from twitbay.users order by username';
										  $query = new Query($sql);
											$userArray = $query->sqlResult($query->sql);
											$users = new ListUsers($userArray);
											$users->listAllUsers($userArray);
										?>
        						<!-- End of user list                    -->
        						<hr />

        						<div class="profile-subpage-call-out">

        							<!--signup form-->
											<?php
												if ($_SESSION["headerContentType"] == 'loggedOut') {
													?>
													<h1>
										        <span>Sign up now!</span>
										      </h1>
													<form action="index.php" method="POST">
			        							Username: <div align="right>"><input type="text" name="username"/><br></div>
														Email: <div align="right>"><input type="email" name="email"/><br></div>
			        							Password: <div align="right>"><input type="text" name="password"/><br></div>
			        							<input type="submit" value="Sign Up!" name=signup />
			        						</form>
													<hr />
													<?php
												}

												require_once('Query.php');

												if (isset($_POST['signup'])) {
													echo "Thanks for signing up!";
													$username = $_POST['username'];
													$email = $_POST['email'];
													$password = $_POST['password'];

												  $sql = "INSERT INTO twitbay.users (username, email, password)
														VALUES ('$username', '$email', '$password')";
												  $query = new Query($sql);
													$query->executeOnly($query->sql);
													$_SESSION["headerContentType"] == 'loggedIn';
												}

											// end of signup form  -->

											// posting form -->

												if ($_SESSION["headerContentType"] == 'loggedIn') {
													?>
													<h1>
										        <span>Make a post!</span>
										      </h1>
													<form action="index.php" method="POST">
			        							Post: <div align="right>"><textarea name="content" rows="10" cols="35">What do you have for sale?</textarea></div>
			        							<input type="submit" value="Post!" name='post'/>
			        						</form>

													<?php
													// Cart  -->
													require_once('ShowCart.php');
													require_once('Query.php');
													require_once('ShowPosts.php');

													$cart = new ShowCart($userID);
													$cart->showCartDetails();

												}

														if (isset($_POST["post"])) {
															$postContent = $_POST['content'];
															$dateTime = date("Y-m-d H:i:s");
															$sql = "INSERT INTO twitbay.posts (userID, content, time_posted) VALUES ('$userID', '$postContent', '$dateTime')";
															$query = new Query($sql);
															$query->executeOnly($query->sql);
														}

													?>

        							<!-- End of cart details -->

        						</div>
        					</div>
        					<hr class="component-spacer">
        				</div>

                  <!--display site stats pulled from database -->
									<?php
										require_once('Stats.php');

										$stats = new Stats();
										$stats->showStats();
									?>
    							<!-- end of stats -->

    							<hr class="component-spacer">
    						</div>
      				</div>
				</div>
				<hr class="component-spacer">
			</div>
		</div>
		<div class="component">
			<hr class="component-spacer">
		</div>
		<!--[if lte IE 6]>
  		<script>using('bundle/ie6').load();</script>
		<![endif]-->
	</body>
</html>
