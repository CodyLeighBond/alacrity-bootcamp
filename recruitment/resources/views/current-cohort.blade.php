@extends('layouts.master')

@section('content')
  <h1>Current Cohort</h1>
  <br>

  <div class="col-md-4">
    <div class="panel">
      <div class="panel panel-body" style="text-align: center">
        <h2>Team 1</h2>
        <br>
        <ul class="person-list">
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 1</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 2</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 3</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 4</li>
        </ul>
        <br>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel">
      <div class="panel panel-body" style="text-align: center">
        <h2>Team 2</h2>
        <br>
        <ul class="person-list">
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 1</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 2</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 3</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 4</li>
        </ul>
        <br>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="panel">
      <div class="panel panel-body" style="text-align: center">
        <h2>Team 3</h2>
        <br>
        <ul class="person-list">
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 1</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 2</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 3</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-user"></i> Person 4</li>
        </ul>
        <br>
      </div>
    </div>
  </div>
@endsection
