@extends('layouts.master')
@section('content')

<br>
<h3>All Applicants</h3>

<!-- Multiple Radios -->
<div class="col-md-2">
  <form method="get" action={{ route('person-filtered')}}>
    {{ csrf_field() }}

    <br><br>
    <div class="form-group">
      <label class="form-label">Select:</label>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="status[]" id="status[]" value="In Progress">
          In Progress
        </label>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="status[]" id="status[]" value="Rejected">
          Rejected
        </label>
      </div>
      <div class="checkbox ">
        <label>
          <input type="checkbox" name="status[]" id="status[]" value="Hired">
          Hired
        </label>
      </div>
    </div>

    From :
    <input type="date" class="form-control" name="fromDate" id="fromDate" placeholder="dd/mm/YYYY" style="font-size: 0.8rem">
    To:
    <input type="date" class="form-control" name="toDate" id="toDate" placeholder="dd/mm/YYYY" style="font-size: 0.8rem">
    <button type="submit" class="btn btn-dash" style="margin-left:0" name="filter">Filter</button>
  </form>

</div>

<!-- Search for names -->
<div class="col-md-2" style="margin-left:20px;">
  <br>
  <input type="text" id="search" onkeyup="tableSearch()" placeholder="Search for names..">
  <br>
</div>

<!-- Table -->
<div class="col-md-8" style="margin-left:20px;">
  <table class="table table-hover table-responsive" id="peopleTable">
    <thead>
      <tr>
        <td>Name</td>
        <td>Application Status</td>
        <td>Submission Date</td>
        <td>Lead Type</td>
        <td>CV</td>
      </tr>
    </thead>
    <tbody>

      @if (isset($people))
      @foreach($people as $person)
        <tr>
          <td><a href="{{ $person->url }}">{{ $person->name }} {{ $person->surname }}</a></td>
          <td>{{ $person->status }}</td>
          <td>{{ $person->created_at }}</td>
          <td>{{ $person->lead_type }}</td>
          <td><a class="btn btn-default" href="#" role="button">CV</a></td>
        </tr>
      @endforeach
      @endif

    </tbody>
  </table>
</div>

@endsection


@section('scripts')
  <script>
    function tableSearch() {
      // Declare variables
      var input, filter, table, tr, td, i;
      input = document.getElementById("search");
      filter = input.value.toUpperCase();
      table = document.getElementById("peopleTable");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  </script>

  @endsection
