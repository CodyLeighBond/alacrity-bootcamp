@extends('layouts.master')
@section('content')

  <div class="col-md-4">
    <br><br>
    <div class="panel">
      <div class="panel panel-body" style="text-align: center">
        <div class="circle-avatar" style="background-image:url('/{{ $person->photo }}')"></div>
        <h2>{{ $person->name}} {{ $person->surname }}</h2>


        @if ($person->status == 'Rejected')
          <h5><span class="label label-danger">Rejected</span></h5>
        @endif

        <br>
        <ul class="person-list">
          <li class="person-list-item"><i class="glyphicon glyphicon-flash"></i> Status: {{ $person->status }}</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-envelope"></i> {{ $person->email }}</li>
          <li class="person-list-item"><i class="glyphicon glyphicon-earphone"></i>  {{ $person->phone }}</li>
          @if ($person->CV)
          <li>
            <a class="btn btn-default" href="/{{ $person->CV }}" target="_blank">CV</a>
          </li>
          @else
          <li>
            <a class="btn btn-default disabled" disabled="disabled" href="/{{ $person->CV }}" target="_blank">CV</a>
          </li>
          @endif
          <li>
            <form  method="post" action={{ route('person.destroy', $person->id) }}>
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button class="btn btn-dash" type="submit">Delete Profile</button>
            </form>
          </li>
        </ul>
        <br>
      </div>
    </div>
  </div>

  <div class="col-md-8">
    <br><br>
    <div class="row bs-wizard hidden-xs" style="border-bottom:0;">

      @if ($person->status != 'Rejected')
        <div class="col-xs-3 bs-wizard-step complete">
      @else
      <div class="col-xs-3 bs-wizard-step disabled">
      @endif
        <div class="text-center bs-wizard-stepnum">Successful Submission</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <i class="bs-wizard-dot"></i>
      </div>

      @if ($person->application->interview()->count() > 1 && $person->status == 'In Progress'
        || $person->status == 'Offer Made' || $person->status == 'Hired'
        || $person->status == 'Offer Rejected')
        <div class="col-xs-3 bs-wizard-step complete">
      @else
      <div class="col-xs-3 bs-wizard-step disabled">
      @endif
        <div class="text-center bs-wizard-stepnum">Interviews In Progress</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <i class="bs-wizard-dot"></i>
      </div>

      @if ($person->application->offer == 1)
        <div class="col-xs-3 bs-wizard-step complete">
      @else
      <div class="col-xs-3 bs-wizard-step disabled">
      @endif
        <div class="text-center bs-wizard-stepnum">Offer Made</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <i class="bs-wizard-dot"></i>
      </div>

      @if ($person->status == 'Hired')
        <div class="col-xs-3 bs-wizard-step complete">
      @else
      <div class="col-xs-3 bs-wizard-step disabled">
      @endif
        <div class="text-center bs-wizard-stepnum">Hired</div>
        <div class="progress"><div class="progress-bar"></div></div>
        <i class="bs-wizard-dot"></i>
      </div>
    </div>

    <div class="notice" style="text-align: center">
      <br><br>
      <!-- If there is an interview in the future, write the date and time here -->
      @if ($person->application->interview->last()->date > Carbon\Carbon::now()
        && $person->application->interview->last()->state == 'Scheduled'
        || $person->application->interview[0]->state == 'Pending'
        && $person->application->offer != 1)
        <h5><span class="label label-action">
          Interview with {{$person->application->interview->last()->lead_interviewer}}
          on {{$person->application->interview->last()->date}}
          at {{$person->application->interview->last()->time}}
        </span></h5>
        <br><br>
      @endif

      @if ($person->status != 'Rejected' && $person->application->interview->last()->state == 'Unscheduled'
      || $person->application->interview->last()->state == 'Passed'
      && $person->application->offer != 1)
      <button class="btn btn-default" type="submit">
        <a style="color:white" href={{ route('interview.index' )}}>Schedule Interview</a>
      </button>
      @else
        <a class="btn btn-default disabled" disabled = "disabled" style="color:white" href={{ route('interview.index' )}}>Schedule Interview</a>
      @endif

      @if ($person->application->interview->last()->state == 'Pending' && $person->application->offer != 1)
        <button class="btn btn-default" type="submit" data-toggle="modal" data-target="#update-progress">Update Progress</button>
      @else
        <button class="btn btn-default disabled" disabled="disabled" type="submit" data-toggle="modal" data-target="#update-progress">Update Progress</button>
      @endif

      @if ($person->application->interview->last()->state == 'Passed' && $person->application->offer != 1)
      <button class="btn btn-default" type="submit" data-toggle="modal" data-target="#offer-made">
        Offer Made
      </button>
      @else
        <button class="btn btn-default disabled" disabled="disabled" type="submit" data-toggle="modal" data-target="#offer-made">
          Offer Made
        </button>
      @endif

      @if ($person->application->offer == 1 && $person->status != 'Hired' && $person->status != 'Offer Rejected')
      <button class="btn btn-default" type="submit" data-toggle="modal" data-target="#offer-accepted">
        Offer Accepted
      </button>
      @else
        <button class="btn btn-default disabled" disabled="disabled" type="submit" data-toggle="modal" data-target="#offer-accepted">
          Offer Accepted
        </button>
      @endif

    </div>
    <br><br><br><br>
  </div>

  <div class="col-md-4 col-md-offset-1 ">
    <h4>Full Details</h4>
    <table class="table table-responsive">
      <tbody>
        <tr>
          <td>Name</td>
          <td>{{ $person->name}} {{$person->surname }}</td>
        </tr>
        <tr>
          <td>Lead Type</td>
          <td>{{ $person->lead_type }}</td>
        </tr>
        <tr>
          <td>Status</td>
          <td>{{ $person->status }}</td>
        </tr>
        <tr>
          <td>Date of Birth</td>
          <td>{{ $person->birthdate }}</td>
        </tr>
        <tr>
          <td>Nationality</td>
          <td>{{ $person->nationality }}</td>
        </tr>
        <tr>
          <td>Welsh?</td>
          <td>{{ $person->welsh?'Yes':'No' }}</td>
        </tr>

      </tbody>
    </table>

    <button class="btn btn-default" type="submit">
      <a style="color:white" href={{ route('person-update', $person) }}>Update Details</a>
    </button>

   </div>


  <!-- Modals -->
<div id="update-progress" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="width: 300px">
      <div class="modal-body" style="text-align:center">
        <form method="post" action={{ route('interview.update', $interview) }}>
          {{ method_field('PATCH') }}
          {{ csrf_field() }}

          <button class="btn btn-default" type="submit" name="state" value="Passed">Progress to Next Stage</button>
          <br>
          <button class="btn btn-dash" type="submit" name="state" value="Failed">Reject</button>
          <br>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <br>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="offer-made" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="width: 300px">
      <div class="modal-body" style="text-align:center">
        <form method="post" action="{{ route('person.setOffer', $person->id) }}">
          {{ method_field('PATCH') }}
          {{ csrf_field() }}

          <p>Has an offer been made to {{ $person->name }}?</p>
          <button class="btn btn-default" type="submit" name="status" value="Offer Made">Yes</button>
          <button class="btn btn-dash" data-dismiss="modal">No</button>
          <br>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="offer-accepted" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="width: 300px">
      <div class="modal-body" style="text-align:center">
        <form method="post" action="{{ route('person.setOfferReponse', $person->id) }}">
          {{ method_field('PATCH') }}
          {{ csrf_field() }}

          <p>Has the offer been accepted by {{ $person->name }}?</p>
          <button class="btn btn-default" type="submit" name="status" value="Hired">Yes</button>
          <button class="btn btn-dash" type="submit" name="status" value="Offer Rejected">No</button>
          <br>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <br>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
