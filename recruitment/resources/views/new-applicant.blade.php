@extends('layouts.master')

@section('content')

<div class="col-md-10">
  <br><br>
  <div class="panel panel-default">
    <div class="panel panel-body form">
      <h3>New Applicant</h3>
      <br>

      @if ($errors->any())

        <ul>
          @foreach ($errors->all() as $error)
          <div class="alert alert-danger">
            <li>{{ $error }}</li>
          </div>
          @endforeach
        </ul>

      @endif

      <form method="post" action="/application" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group form-inline">
          <label class="sr-only" for="name">Name</label>
          <input type="text" class="form-control" id="name" placeholder="Name" name="name">
          <label class="sr-only" for="surname">Surname</label>
          <input type="text" class="form-control" id="surname" placeholder="Surname" name="surname">
        </div>

        <br>

        <div class="form-group">
          <label class="form-label" for="birthdate">Date of Birth</label>
          <input type="date" class="form-control" id="birthdate" placeholder="dd/mm/YYYY" name="birthdate">
        </div>

        <div class="form-group">
          <label class="form-label" for="nationality">Nationality</label>
          <input type="text" class="form-control" id="nationality" placeholder="e.g British" name="nationality">
        </div>

        <div class="form-group">
          <label class="form-label" for="email">Applicant Email</label>
          <input type="email" class="form-control" id="email" placeholder="Email" name="email">
        </div>

        <div class="form-group">
          <label class="form-label" for="phone">Applicant Phone Number</label>
          <input type="text" class="form-control" id="phone" placeholder="Phone" name="phone">
        </div>

        <div class="form-group">
          <label class="form-label" for="applicationSource">Application Source</label>
          <input type="text" class="form-control" id="applicationSource" placeholder="eg. LinkedIn" name="source">
        </div>

        <div class="form-group">
          <label class="form-label" for="lead_type">Lead Type</label>
          <input type="text" class="form-control" id="lead_type" placeholder="Business/Technical/Tybrid" name="lead_type">
        </div>
        <br>

        <div class="form-group">
          <label class="form-label" for="CV">Upload CV</label>
          <input type="file" id="CV" name="cv">
        </div>
        <br>

        <div class="form-group">
          <label class="form-label" for="photo">Upload Photo</label>
          <input type="file" id="photo" name="photo">
        </div>
        <br>

        <div class="form-group">
          <label class="form-label" class="control-radio" for="visa">VISA?</label>
          <div class="radio">
            <label class="radio">
              <input type="radio" id="optionsRadios1" value="1" name="visa">
              Yes
            </label>
          </div>
          <div class="radio">
            <label class="radio">
              <input type="radio" id="optionsRadios2" value="0" name="visa">
              No
            </label>
          </div>
        </div>
        <br>

        <div class="form-group">
          <label class="form-label" for="good_candidate">Good Candidate?</label>
          <div class="radio">
            <label>
              <input type="radio" id="optionsRadios1" value="1" name="good_candidate">
              Yes
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" id="optionsRadios2" value="0" name="good_candidate">
              No
            </label>
          </div>
        </div>
        <br>

        <div class="form-group">
          <label class="form-label" for="welsh">Welsh?</label>
          <div class="radio">
            <label>
              <input type="radio" id="optionsRadios1" value="1" name="welsh">
              Yes
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" id="optionsRadios2" value="0" name="welsh">
              No
            </label>
          </div>
        </div>
        <button type="submit" class="btn btn-default" name="save">Save</button>
      </form>
    </div>
    </div>
    <br><br>
  </div>

@endsection
