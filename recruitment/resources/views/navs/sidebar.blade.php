<div class="col-md-2">
  <br><br>
  <div class="panel panel-default hidden-xs" style="height: 100%;">
    <div class="panel-heading dash-panel-head" style="text-align: center">
      <h5>Actions</h5>
    </div>
    <div class="panel-body">
      <ul class="dash-list">
        <li class="dash-list-item">
          Inverviews to schedule
          <span class="badge sidebar-badge">
            {{ App\Http\Controllers\InterviewController::getScheduleCount() }}
            <!-- count the number of most recent interviews for a person where status != pending -->
          </span>
        </li>
        <li class="dash-list-item">
          Interviews to review
          <span class="badge sidebar-badge">
            {{ App\Http\Controllers\InterviewController::getReviewCount() }}
            <!-- fix this route -->
          </span>
        </li>
      </ul>
    </div>
  </div>
</div>
