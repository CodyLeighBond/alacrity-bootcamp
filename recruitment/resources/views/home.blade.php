@extends('layouts.dashboard')


@section('content')
<body style="background-color: #80cbc4">
  <div class="container">
    <br><br><br><br>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif


  </div>

  @if (Auth::guest())
      <p><a class="btn btn-dash btn-lg" href="login" role="button">Log In</a></p>
  @else
      <div class="dash-text-primary" >
        <h1 class="dash-text-primary">Welcome</h1>

        <form id="logout-form" action="{{ route('login') }}"method="POST">
            {{ csrf_field() }}

            <p><a class="btn btn-dash btn-lg" href="{{ route('login') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();" role="button">Log Out</a></p>
        </form>
      </div>
  @endif

</body>
@endsection
