@extends('layouts.master')

@section('content')

<!-- Change state of interview to pending if it is in the past -->
@if (isset($interviews))
  @foreach($interviews as $interview)
    @if ($interview->date < Carbon\Carbon::now() && $interview->state == 'Scheduled')
      {{ App\Http\Controllers\InterviewController::setPending($interview) }}
    @endif
  @endforeach
@endif

  <div class="col-md-7 ">
    <h3>Upcoming Interviews</h3>
    <table class="table table-hover table-responsive" style="width:100%">
      <thead>
        <tr>
          <td>Date</td>
          <td>Time</td>
          <td>Lead Interviewer</td>
          <td>Applicant</td>
          <td>  </td>
        </tr>
      </thead>
      <tbody>

        @if (isset($interviews))
          @foreach($interviews as $interview)
            @if ($interview->date >= Carbon\Carbon::now())
              <tr>
                <td><i class="glyphicon glyphicon-calendar"></i> {{ $interview->date }}</td>
                <td><i class="glyphicon glyphicon-time"></i> {{ $interview->time }}</td>
                <td> {{ $interview->lead_interviewer }}</td>
                <td><a href="{{ $interview->application->person->url }}">
                  {{ $interview->application->person->name }} {{ $interview->application->person->surname }}
                </a></td>
                <td>
                  <form style="vertical-align:top" method="post" action={{ route('interview.destroy', $interview->id) }}>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-default btn-xs" type="submit"><span class="glyphicon glyphicon-trash" style="color:white" ></span></button>
                  </form>
                </td>
              </tr>
            @endif
          @endforeach
        @endif

      </tbody>
    </table>
    <br><br>

    <h3>Interviews to Review</h3>
    <table class="table table-hover table-responsive" style="width:100%">
      <thead>
        <tr>
          <td>Date</td>
          <td>Time</td>
          <td>Lead Interviewer</td>
          <td>Applicant</td>
          <td>  </td>
        </tr>
      </thead>
      <tbody>

        @if (isset($interviews))
          @foreach($interviews as $interview)
            @if (($interview->date < Carbon\Carbon::now()) && ($interview->state == 'Pending'))
              <tr>
                <td><i class="glyphicon glyphicon-calendar"></i> {{ $interview->date }}</td>
                <td><i class="glyphicon glyphicon-time"></i> {{ $interview->time }}</td>
                <td> {{ $interview->lead_interviewer }}</td>
                <td><a href="{{ $interview->application->person->url }}">
                  {{ $interview->application->person->name }} {{ $interview->application->person->surname }}
                </a></td>
                <td>
                  <form style="vertical-align:top" method="post" action={{ route('interview.destroy', $interview->id) }}>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-default btn-xs" type="submit"><span class="glyphicon glyphicon-trash" style="color:white" ></span></button>
                  </form>
                </td>
              </tr>
            @endif
          @endforeach
        @endif

      </tbody>
    </table>
  </div>

  <div class="col-md-3 col-md-offset-1">
    <br><br>
    <div class="panel panel-default" >
      <div class="panel-heading interview-panel-head" style="text-align: center">
        <h5>Schedule Interview</h5>
      </div>
      <div class="panel-body">

        <form method="post" action="/interview">
          {{ csrf_field() }}

          <div class="form-group">
            <label class="sr-only" for="name">Name</label>
            <input type="text" class="form-control" id="name" placeholder="Name" name="name">
            <label class="sr-only" for="surname">Surname</label>
            <input type="text" class="form-control" id="surname" placeholder="Surname" name="surname">
            <br>
            <label class="form-label" for="email">Applicant Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email" name="email">
            <label class="form-label" for="date">Date  </label>
            <input type="date" class="form-control" id="date" placeholder="dd/mm/YYYY" name="date">
            <label class="form-label" for="time">Time  </label>
            <input type="time" class="form-control" id="interviewTime" placeholder="" name="time">
            <label class="form-label" for="type">Type  </label>
            <input type="text" class="form-control" id="type" placeholder="e.g Technical" name="type">
            <label class="form-label" for="lead_interviewer">Lead Interviewer  </label>
            <input type="text" class="form-control" id="lead_interviewer" placeholder="Full name" name="lead_interviewer">
            <br>
          </div>
          <div class="form-group" style="text-align:center" >
            <button type="submit" class="btn btn-dash" name="save">Save</button>
          </div>
        </form>

      </div>
    </div>
  </div>

  <!-- Calendar  -->
  <!-- <div class="col-md-8">
    <div class="responsive-iframe-container big-container hidden-xs">
      <iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;height=900&amp;wkst=2&amp;bgcolor=%23FFFFFF&amp;src=en.uk%23holiday%40group.v.calendar.google.com&amp;color=%2329527A&amp;ctz=Europe%2FLondon"
        style="border-width:0" width="1200" height="900" frameborder="0" scrolling="no">
      </iframe>
    </div>
    <div class="responsive-iframe-container small-container hidden-lg hidden-md">
      <iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTz=0&amp;mode=AGENDA&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=en.uk%23holiday%40group.v.calendar.google.com&amp;color=%2329527A&amp;ctz=Europe%2FLondon"
        style="border-width:0" width="100%" height="600" frameborder="0" scrolling="no">
      </iframe>
    </div>
  </div> -->
@endsection
