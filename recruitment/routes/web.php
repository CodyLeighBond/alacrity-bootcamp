<?php

use Illuminate\Http\Request;
use App\Person;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Routes for the main views
Route::get('update-applicant', function() {
  return view('update-applicant');
})->name('update-applicant');

//Routes for application model
Route::resource('application', 'ApplicationController');

//Routes for person model
Route::get('person/update/{person}', 'PersonController@showUpdate')->name('person-update');
Route::get('person/filtered', 'PersonController@filterResults')->name('person-filtered');
Route::patch('person/{person}/updateOffer', 'PersonController@setOffer')->name('person.setOffer');
Route::patch('person/{person}/setOfferReponse', 'PersonController@setOfferReponse')->name('person.setOfferReponse');
Route::get('photos/{photo}', 'PersonController@showPhoto');
Route::get('cvs/{cv}', 'PersonController@showCV');

Route::resource('person', 'PersonController');



//Routes for interview model
Route::resource('interview', 'InterviewController');


//Routes for authentication
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
