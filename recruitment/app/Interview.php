<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
  protected $fillable = [
      'date', 'time', 'type', 'state', 'Lead Interviewer'
  ];

  public function application() {
    return $this->belongsTo('App\Application');
  }

}
