<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
  protected $fillable = ['application_id', 'datetime', 'type', 'state'];


  public function application()
    {
        return $this->belongsTo('App\Application');
    }
}

?>
