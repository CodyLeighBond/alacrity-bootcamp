<?php

namespace App\Http\Controllers;

use App\Person;
use App\Interview;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Carbon;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $interviews = Interview::get();
      //dd($interviews);
      return view('interview-schedule', compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         //validate request
         $this->validate($request, [
           'name' => 'required|min:3|max:120',
           'surname' => 'required|min:3|max:120',
           'date' => 'required',
           'time' => 'required',
           'type' => 'required'
         ]);

         // create a new person with form request data
         $interview = new \App\Interview;

         //Find person and application matching the input name
         $person = Person::whereEmail(request('email'))->get();

         $interview->application_id = $person[0]->application->id;
         $interview->date = request('date');
         $interview->time = request('time');
         $interview->type = request('type');
         $interview->lead_interviewer = request('lead_interviewer');
         $interview->state = 'Scheduled';
         $interview->save();

         return redirect('interview');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
     public function show(Interview $interview)
     {
         // GET/applications/id
         return view('interview-schedule');
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    // public function edit(Interview $interview)
    // {
    //   if ($request->has('date')) {
    //       $interview->email = request('date');
    //   }
    //
    //   $interview->application_id = $person[0]->application->id;
    //   $interview->date = request('date');
    //   $interview->time = request('time');
    //   $interview->type = request('type');
    //   $interview->lead_interviewer = request('lead_interviewer');
    //   $interview->state = 'Scheduled';
    //   $interview->save();
    //
    //   return redirect('interview');
    //
    //   $person->save();
    //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Interview $interview)
    {
      $interview->state = request('state');
      $interview->save();

      $person = $interview->application->person;

      if (request('state') == "Failed") {
        $interview->application->person->status = "Rejected";
      }
      $person->save();

      return redirect()->route('person.show', $interview->application->person->id);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview $interview)
    {
      $interview->delete();
      return redirect('interview');
    }

    public static function getReviewCount()
    {
      $interviews = Interview::where('state', 'Pending')->get();
      return count($interviews);
    }

    // public static function getScheduleCount()
    // {
    //   $applications = DB::select('select distinct application_id from interviews');
    //   $numApplications = count($applications);
    //
    //   for($i=1; $i<=$numApplications; $i++){
    //
    //     $interviews[$i] = DB::select('
    //       select state
    //       from interviews
    //       join applications on interviews.application_id = applications.id
    //       where interviews.application_id = ? and isnull(applications.offer)
    //       and applications.good_candidate = 1 and applications.visa = 1
    //       order by interviews.created_at desc
    //       limit 1',
    //       [$i]);
    //   }
    //   $count = 0;
    //   for ($i=1; $i<count($interviews); $i++){
    //     if ($interviews[$i][0]->state != "scheduled"){
    //       $count += 1;
    //     }
    //   }
    //   return $count;
    //   }

    public static function getScheduleCount()
    {
      $applicationsWithNoOffer = Application::whereOffer(null)->get();

      $applicationsNeedingAnInterview = $applicationsWithNoOffer->filter(function (Application $application) {
        $mostRecentInterview = $application
          ->interview()
          ->latest()
          ->first();

        $interviewIsPassedOrUnscheduled = $mostRecentInterview->state == 'Unscheduled' || $mostRecentInterview->state == 'Passed';
        return $interviewIsPassedOrUnscheduled;
      });

      return $applicationsNeedingAnInterview->count();
    }

    public static function setPending(Interview $interview)
    {
      $interview->state = 'Pending';
      $interview->save();


    }

}
