<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use \App\Person;
use \App\Interview;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
      {
        // extract the posts from the database!
        $people = Person::get();
        return view('current-applicants', compact('people'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //  public function show($id)
    //  {
    //      // GET/applications/id
    //      return view('person/($id)');
    //  }

     public function show(Person $person)
      {
        $interview  = Interview::find($person->application->interview->last()->id );

        return view('person', compact('person', 'interview'));
      }

      public function showUpdate(Person $person)
       {
         return view('update-applicant', compact('person'));
       }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
      if ($request->has('name')) {
          $person->name = request('name');
      }
      if ($request->has('surname')) {
          $person->name = request('surname');
      }
      if ($request->has('birthdate')) {
          $person->name = request('birthdate');
      }
      if ($request->has('nationality')) {
          $person->name = request('nationality');
      }
      if ($request->has('welsh')) {
          $person->name = request('welsh');
      }
      if ($request->has('email')) {
          $person->name = request('email');
      }
      if ($request->has('phone')) {
          $person->name = request('phone');
      }
      if ($request->has('lead_type')) {
          $person->name = request('lead_type');
      }
      if ($request->has('cv')) {
          $person->name = request('cv');
      }
      if ($request->has('photo')) {
          $person->name = request('photo');
      }

      $person->save();

      // create a new application with form request data
      $application = $person->application;

      $application->person_id = $person->id;
      if ($request->has('visa')) {
          $person->name = request('visa');
      }
      if ($request->has('good_candidate')) {
          $person->name = request('good_candidate');
      }
      if ($request->has('source')) {
          $person->name = request('source');
      }

      $application->save();

      return redirect($person->url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        $person->delete();
        $person->application->delete();
        $person->application->interview()->delete();
        return redirect()->route('person.index');
    }

    public function filterResults() {
      $statusArray = request('status');

      if (count($statusArray)==2) {
        $statusArray[2] = null;
      }
      elseif (count($statusArray)==1) {
        $statusArray[1] = null;
        $statusArray[2] = null;
      }

      if(request('fromDate') == null) {
        $fromDate = '1901-01-01';
      }
      else {
        $fromDate = request('fromDate');
      }

      if(request('toDate') == null) {
        $toDate = '3000-01-01';
      }
      else {
        $toDate = request('toDate');
      }

      $people = DB::select('
        select *
        from people
        join applications on applications.person_id = people.id
        where applications.created_at > ?
        and applications.created_at < ?
        and people.status = ?
        or people.status = ?
        or people.status = ?',
        [$fromDate, $toDate, $statusArray[0], $statusArray[1], $statusArray[2]]
      );
      //dd($statusArray);
      $peopleArray = [];

      for ($i=0; $i<count($people); $i++){
        $peopleArray[$i]  = Person::find($people[$i]->id );
      }

      $people = collect($peopleArray);

       return view('current-applicants', compact('people'));
    }

    public function setOffer(Person $person)
    {
      $person->status = 'Offer Made';
      $person->save();

      $application = $person->application;
      $application->offer = 1;
      $application->save();

      return redirect($person->url);
    }

    public function setOfferReponse(Request $request, Person $person)
    {
      $person->status = request('status');
      $person->save();

      return redirect($person->url);
    }

    public function showPhoto($photoId)
    {
      $file = Storage::get('photos/' . $photoId);
      return response($file, 200);
    }

    public function showCV($cvId)
    {
      $file = Storage::get('cvs/' . $cvId);
      $response = Response::make($file, 200);
      $response->header("Content-Type", 'application/pdf');
      return $response;
    }

}
