<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('new-applicant');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate request
        $this->validate($request, [
          'name' => 'required|min:3|max:120',
          'surname' => 'required|min:3|max:120',
          'birthdate' => 'required',
          'nationality' => 'required',
          'welsh' => 'required',
          'email' => 'required|min:3|max:240',
          'phone' => 'required',
          'lead_type' => 'required',
          'source' => 'required'
        ]);

        // create a new person with form request data
        $person = new \App\Person;

        if ((request('good_candidate') == 0) || (request('visa') == 0)){
          $status = 'Rejected';
        }
        else {
          $status = 'In Progress';
        }

        if ($request->hasFile('cv')) {
          $cv = $request->cv->store('cvs');
          $person->cv = $cv;
        }

        if ($request->hasFile('photo')) {
          $photo = $request->photo->store('photos');

        }
        else {
          $photo = 'photos/placeholder.png';
        }

        $person->name = request('name');
        $person->surname = request('surname');
        $person->birthdate = request('birthdate');
        $person->nationality = request('nationality');
        $person->welsh = request('welsh');
        $person->email = request('email');
        $person->phone = request('phone');
        $person->lead_type = request('lead_type');
        $person->status = $status;
        $person->photo = $photo;

        $person->save();

        // create a new application with form request data
        $application = new \App\Application;

        $application->person_id = $person->id;
        $application->visa = request('visa');
        $application->good_candidate = request('good_candidate');
        $application->source = request('source');

        $application->save();

        // create a new interview with form request data
        $interview = new \App\Interview;

        $interview->application_id = $application->id;
        $interview->state = 'Unscheduled';

        $interview->save();

        // redirect to all applicants page for now,
        //but change this to a redirect to the person's
        //profile when that is possible
        return redirect($person->url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        // GET/applications/id
        return view('current-applicants');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // GET/applications/id/edit
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // PATCH/applications/id
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DELETE/applications/id
    }
}
