<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
  protected $fillable = [
      'name', 'surname', 'birthdate', 'nationality', 'welsh', 'CV', 'email', 'phone', 'lead_type', 'photo'
  ];

  public function application() {
    return $this->hasOne('App\Application');
  }

  public function getUrlAttribute() {
    return route('person.show', $this->id);
  }
}
