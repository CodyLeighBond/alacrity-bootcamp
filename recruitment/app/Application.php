<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
  protected $fillable = [
      'visa', 'good_candidate', 'offer', 'source'
  ];

  public function person()
  {
    return $this->belongsTo('App\Person');
  }

  public function interview()
  {
    return $this->hasMany('App\Interview');
  }
}
