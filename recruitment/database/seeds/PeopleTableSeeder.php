<?php

use Illuminate\Database\Seeder;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert([
          'name' => 'Alex',
          'surname' => 'Evans',
          'birthdate' => '1993-06-29',
          'nationality' => 'British',
          'email' => 'alexevans1234@567.com',
          'phone' => 9583910195,
          'lead_type' => 'Technical',
          'welsh' => 1,
          'status' => 'In Progress',
          'created_at' => Carbon\Carbon::now(),
          'photo' => 'photos/placeholder.png'
        ]);
    }
}
