<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('applications')->insert([
        'person_id' => 1,
        'visa' => 1,
        'good_candidate' => 1,
        'source' => 'Website'
      ]);
    }
}
