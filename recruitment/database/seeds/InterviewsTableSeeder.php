<?php

use Illuminate\Database\Seeder;

class InterviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('interviews')->insert([
        'application_id' => 1,
        'state' => 'Unscheduled'
      ]);
    }
}
