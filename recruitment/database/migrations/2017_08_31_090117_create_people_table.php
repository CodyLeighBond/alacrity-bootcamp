<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 120);
            $table->char('surname', 120);
            $table->date('birthdate');
            $table->char('status', 120)->nullable();
            $table->char('nationality', 240);
            $table->boolean('welsh');
            $table->string('CV')->nullable();
            $table->string('photo')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->char('lead_type', 120);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
