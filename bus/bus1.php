<html>

<?php
//Model the bus
$passengers = [
  'Alice',
  'Bob',
  'Carol'
];
$busCapacity =5;

//Model the queue
$queue = [
  'Dave',
  'Eve',
  'Fred'
];

//Model the bus processing the queue
while(count($passengers) < $busCapacity && count($queue) > 0){
  $passengers[] = array_pop($queue);
}
?>

<p>
  After stopping at the stop, the passengers on the bus are:
  <ul>
    <?php
    foreach ($passengers as $passenger) {
      echo "<li>$passenger</li>";
    }
     ?>
  </ul>
  The people waiting are:
  <ul>
    <?php
    foreach ($queue as $person) {
      echo "<li>$person</li>";
    }
     ?>

</html>
