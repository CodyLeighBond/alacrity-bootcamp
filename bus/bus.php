<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Bus</title>
  </head>
  <body>
    <?php
      $seatCapacity = 5;
      $onBus = ['Aaron', 'Adam', 'Andrew', 'Alex'];
      $atBusStop = ['Marty', 'Jock', 'Einstein'];
      $allPeople = array_merge($onBus, $atBusStop);
      $total = count($onBus) + count($atBusStop);
      $difference = abs($total-$seatCapacity);

      echo "Currently on the bus: ";
      for ($i=0; $i < count($onBus); $i++) {
        echo "<br>" . $onBus[$i];
      }

      if ($total < $seatCapacity) {
        echo "<br><br>There are $difference seats remaining";
      }
      elseif ($total > $seatCapacity) {
        echo "<br><br>There are $difference people left waiting:";
        $peopleLeft = [];
        $j = 0;

        for ($i=$seatCapacity; $i < count($allPeople); $i++) {
          $peopleLeft[$j] = $allPeople[$i];
          $j++;
          echo "<br>" . $allPeople[$i];
        }
      }
      else {
        echo "There are no seats remaining";
      }

     ?>
  </body>
</html>
  
