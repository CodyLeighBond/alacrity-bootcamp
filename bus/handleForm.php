<!DOCTYPE html>
<?php
  //Extract the info from the request
  $capacity = $_GET['capacity'];

  require_once('queueClass.php');

  $initialQueue = [
    'Adam',
    'Bob',
    'Carol',
    'Daniel',
    'Evan',
    'Frank',
    'George'
  ];

  $queue = new Queue($initialQueue);
  $peopleInQueue = count($initialQueue);
 ?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Passengers</title>
  </head>
  <body>
    <h1></h1>
    <form class="Form" action="transportReporter.php" method="GET">
      There are <?=$peopleInQueue?> people in the queue, how many do you want to let on?
      <input type="text" name="allowedOn">
      <input type="submit" name="enter" value="enter">


      <!-- We will hide the data from the previous form, so we can pass it on..  -->
      <input type="hidden" name="capacity" value="<?= $capacity ?>">
    </form>
  </body>
</html>
