<?php
  class Bus {
    public $maxCapacity;
    public $getOff;
    public $passengers;

    function __construct(int $maxCapacity = 50,  array $passengers = []) {
      if (count($passengers) > $maxCapacity) {
        $maxCapacity = count($passengers);
      }

      $this->maxCapacity = $maxCapacity;
      $this->passengers = $passengers;
    }

    // Getter
    public function getPassengers() {
      return $this->passengers;
    }

    //Setter
    public function addPassenger(string $passenger) {
      $this->passengers[] = $passenger;
    }

    //Setter
    public function removePassenger() {
      for ($i=0; $i <= $this->getOff; $i++) {
        array_shift($this->passengers);
      }
    }

    // Getter
    public function getMaxCapacity() {
      return $this->maxCapacity;
    }

    // Return current number of passengers
    public function numberOfPassengers() {
      return count($this->passengers);
    }

  }
?>
