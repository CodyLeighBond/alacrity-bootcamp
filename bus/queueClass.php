<?php

  class Queue
    {
      public $people;

      function __construct(array $people = []) {
        $this->people = $people;
      }

      // Getter
      public function getPeople() {
          return $this->people;
      }

      //Setter
      public function moveToBus() {
        return array_shift($this->people);

      }

      // Return current number of people queuing
      public function totalInQueue() {
        return count($this->people);
      }
    }
