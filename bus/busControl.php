<html>

  <?php
    require_once('busClass.php');
    require_once('queueClass.php');
    require_once('driverClass.php');

    //Model the bus
    $initialPassengers = [
      'Alice',
      'Bob',
      'Carol',
      'Dave',
    ];

    $busCapacity =5;

    //Model the queue
    $initialQueue = [
      'Eve',
      'Frank',
      'George',
      'Henry'
    ];

    $bus = new Bus($busCapacity, $initialPassengers);
    $queue = new Queue($initialQueue);
    $driver = new Driver($bus, $queue);
    $passengers = $bus->passengers;
    $people = $queue->people;

     echo "<br>Current Passengers:<br>";
     for ($i=0; $i < count($passengers) ; $i++) {
        echo "$passengers[$i]<br>";
    }
    echo "<br>Current People In Queue:<br>";
    for ($i=0; $i < count($people) ; $i++) {
       echo "$people[$i]<br>";
   }

    list($passengers, $people) = $driver->collectPassengers();

    echo "<br>Current Passengers:<br>";
    for ($i=0; $i < count($passengers) ; $i++) {
       echo "$passengers[$i]<br>";
   }
   echo "<br>Current People In Queue:<br>";
   for ($i=0; $i < count($people) ; $i++) {
      echo "$people[$i]<br>";
  }

?>

</html>
