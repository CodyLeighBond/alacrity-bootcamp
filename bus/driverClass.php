<?php

  class Driver {
    private $allowedOn;
    private $bus;
    private $queue;

    function __construct(int $allowedOn, bus $bus, queue $queue){
      $this->allowedOn = $allowedOn;
      $this->bus = $bus;
      $this->queue = $queue;
    }

    public function collectPassengers() {
      for ($i=0; $i < $this->allowedOn; $i++) {
        $this->bus->addPassenger($this->queue->moveToBus());
      }
      return array($this->bus->passengers, $this->queue->people);
    }
  }

?>
