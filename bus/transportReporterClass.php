<?php

  class TransportReporter {
    private $bus;
    private $queue;

    function __construct(Bus $bus, Queue $queue) {
      $this->bus = $bus;
      $this->queue = $queue;
    }

    public function showQueue() {
      $outputHead = '<p>Current people waiting in the queue are:</p>';
      $outputBody = '<ul>';

      foreach ($this->queue->getPeople() as $person) {
        $outputBody .= "<li>$person";
      }

      $outputBody .= "</li></ul>";
      echo $outputHead.$outputBody;
    }

    public function showPassengers() {
      $outputHead = '<p>People currently on the bus are:</p>';
      $outputBody = '<ul>';

      foreach ($this->bus->getPassengers() as $passenger) {
        $outputBody .= "<li>$passenger";
      }

      $outputBody .= "</li></ul>";
      echo $outputHead.$outputBody;
    }

    public function showBusCapacity() {
      $output = '<p>Maximum capacity of the bus is ' . $this->bus->getMaxCapacity();
      echo $output;
    }
  }

?>
