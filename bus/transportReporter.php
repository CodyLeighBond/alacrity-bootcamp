<!DOCTYPE html>
<?php
//Extract the info from the request
$allowedOn = $_GET['allowedOn'];
$capacity = $_GET['capacity'];

require_once('busClass.php');
require_once('queueClass.php');
require_once('driverClass.php');
require_once('transportReporterClass.php');

$initialPassengers = [];
$initialQueue = [
  'Adam',
  'Bob',
  'Carol',
  'Daniel',
  'Evan',
  'Frank',
  'George'
];


$bus = new Bus($capacity, $initialPassengers);
$queue = new Queue($initialQueue);
$transportManager = new Driver($allowedOn, $bus, $queue);
$transportReporter = new TransportReporter($bus, $queue);

$transportReporter->showBusCapacity();
$transportReporter->showPassengers();
$transportReporter->showQueue();
echo('<p>Stopping at the bus-stop...');
$transportManager->collectPassengers();
$transportReporter->showPassengers();
$transportReporter->showQueue();
 ?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
  </head>
  <body>
    <h1 class = 'lead'>Bus Stop Simulator</h1>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
  </body>
</html>
